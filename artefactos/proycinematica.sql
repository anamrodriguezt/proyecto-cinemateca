-- phpMyAdmin SQL Dump
-- version 5.0.3
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 01-02-2021 a las 20:25:26
-- Versión del servidor: 10.4.14-MariaDB
-- Versión de PHP: 7.2.34

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `proycinematica`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `actor`
--

CREATE TABLE `actor` (
  `idactor` int(11) NOT NULL,
  `nombre` varchar(45) NOT NULL,
  `apellido` varchar(45) DEFAULT NULL,
  `nombrePersonaje` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `administrador`
--

CREATE TABLE `administrador` (
  `idadministrador` int(11) NOT NULL,
  `nombre` varchar(45) NOT NULL,
  `apellido` varchar(45) DEFAULT NULL,
  `correo` varchar(45) DEFAULT NULL,
  `clave` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `administrador`
--

INSERT INTO `administrador` (`idadministrador`, `nombre`, `apellido`, `correo`, `clave`) VALUES
(2, 'Diego', 'Fernandez', 'pgl@pgl.com', 'd0d1b3721f62133f8e8b6eb710e58ad1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `carrito`
--

CREATE TABLE `carrito` (
  `idCarrito` int(11) NOT NULL,
  `idCliente` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `carrito_pelicula`
--

CREATE TABLE `carrito_pelicula` (
  `idCarrito_pelicula` int(11) NOT NULL,
  `Cantidad` varchar(45) NOT NULL,
  `idCarrito` int(11) NOT NULL,
  `idpelicula` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cliente`
--

CREATE TABLE `cliente` (
  `IdCliente` int(11) NOT NULL,
  `nombre` varchar(45) NOT NULL,
  `apellido` varchar(45) NOT NULL,
  `correo` varchar(45) NOT NULL,
  `clave` varchar(45) NOT NULL,
  `estado` int(11) NOT NULL,
  `foto` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `cliente`
--

INSERT INTO `cliente` (`IdCliente`, `nombre`, `apellido`, `correo`, `clave`, `estado`, `foto`) VALUES
(1, 'Nicolas', 'Molano', 'nm@nm.com', '93122a9e4abcba124d5a7d4beaba3f89', 1, ''),
(2, 'Diego', 'Lopez', 'dl@dl.com', 'dl', 1, '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `critico`
--

CREATE TABLE `critico` (
  `idcritico` int(11) NOT NULL,
  `nombre` varchar(45) NOT NULL,
  `apellido` varchar(45) NOT NULL,
  `correo` varchar(45) NOT NULL,
  `clave` varchar(45) NOT NULL,
  `foto` varchar(45) DEFAULT NULL,
  `estado` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `critico`
--

INSERT INTO `critico` (`idcritico`, `nombre`, `apellido`, `correo`, `clave`, `foto`, `estado`) VALUES
(3, 'Paola', 'Sanchez', 'ps@ps.com', '8812c36aa5ae336c2a77bf63211d899a', NULL, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `director`
--

CREATE TABLE `director` (
  `idDirector` int(11) NOT NULL,
  `nombre` varchar(45) NOT NULL,
  `apellido` varchar(45) DEFAULT NULL,
  `nacionalidad` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `director`
--

INSERT INTO `director` (`idDirector`, `nombre`, `apellido`, `nacionalidad`) VALUES
(7, 'Jhonny', 'Herrera', 'Colombiano'),
(8, 'Ramon', 'Rodriguez', 'Español');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `funcion`
--

CREATE TABLE `funcion` (
  `idfuncion` int(11) NOT NULL,
  `fechaInicio` date DEFAULT NULL,
  `fechaFin` date DEFAULT NULL,
  `pelicula` int(11) NOT NULL,
  `horario` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `funcion`
--

INSERT INTO `funcion` (`idfuncion`, `fechaInicio`, `fechaFin`, `pelicula`, `horario`) VALUES
(7, '2021-01-23', '2021-01-24', 7, 6);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `genero`
--

CREATE TABLE `genero` (
  `idgenero` int(11) NOT NULL,
  `nombre` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `genero`
--

INSERT INTO `genero` (`idgenero`, `nombre`) VALUES
(7, 'Terror'),
(8, 'Comedia'),
(9, 'Romance');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `horario`
--

CREATE TABLE `horario` (
  `idhorario` int(11) NOT NULL,
  `horaInicio` time DEFAULT NULL,
  `horaFin` time DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `horario`
--

INSERT INTO `horario` (`idhorario`, `horaInicio`, `horaFin`) VALUES
(6, '20:03:35', '00:04:36'),
(7, '15:04:27', '17:05:28');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `idioma`
--

CREATE TABLE `idioma` (
  `ididioma` int(11) NOT NULL,
  `nombre` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `idioma`
--

INSERT INTO `idioma` (`ididioma`, `nombre`) VALUES
(7, 'Español'),
(8, 'ingles');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `logadministrador`
--

CREATE TABLE `logadministrador` (
  `idLogadministrador` int(11) NOT NULL,
  `accion` varchar(45) NOT NULL,
  `fecha` varchar(45) NOT NULL,
  `hora` varchar(45) NOT NULL,
  `idAdministrador` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `logadministrador`
--

INSERT INTO `logadministrador` (`idLogadministrador`, `accion`, `fecha`, `hora`, `idAdministrador`) VALUES
(2, 'Inicio de sesion', '2021-02-01', '03:13:12', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `logcliente`
--

CREATE TABLE `logcliente` (
  `idLogcliente` int(11) NOT NULL,
  `accion` varchar(45) NOT NULL,
  `fecha` varchar(45) NOT NULL,
  `hora` varchar(45) NOT NULL,
  `idCliente` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `logcliente`
--

INSERT INTO `logcliente` (`idLogcliente`, `accion`, `fecha`, `hora`, `idCliente`) VALUES
(1, 'Inicio de sesion', '2021-02-01', '16:10:49', 0),
(2, 'Inicio de sesion', '2021-02-01', '17:23:04', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `logcritico`
--

CREATE TABLE `logcritico` (
  `idLogcritico` int(11) NOT NULL,
  `accion` varchar(45) NOT NULL,
  `fecha` varchar(45) NOT NULL,
  `hora` varchar(45) NOT NULL,
  `idCritico` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `logcritico`
--

INSERT INTO `logcritico` (`idLogcritico`, `accion`, `fecha`, `hora`, `idCritico`) VALUES
(3, 'Inicio de sesion', '2021-02-01', '16:42:59', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pelicula`
--

CREATE TABLE `pelicula` (
  `idpelicula` int(11) NOT NULL,
  `nombre` varchar(45) NOT NULL,
  `sinopsis` varchar(100) DEFAULT NULL,
  `idioma` int(11) NOT NULL,
  `imagen` varchar(45) DEFAULT NULL,
  `genero` int(11) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `precio` int(11) DEFAULT NULL,
  `idDirector` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `pelicula`
--

INSERT INTO `pelicula` (`idpelicula`, `nombre`, `sinopsis`, `idioma`, `imagen`, `genero`, `cantidad`, `precio`, `idDirector`) VALUES
(7, 'Bob esponja al rescate', 'Cuando alguien rapta a Gary, Bob Esponja y Patricio se embarcan en una alocada misión muy lejos de F', 7, 'bobesponja1.jpg', 8, 2, 2000, 8),
(10, 'Jurassic World: el reino caído', 'Ante el peligro de un volcán que está por hac', 8, 'jurassic2.png', 7, 6, 3000, 8),
(16, 'Holidate', 'Cansados de estar solteros en vacaciones, dos', 8, 'holidate3.jpg', 9, 10, 7000, 8);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `reparto`
--

CREATE TABLE `reparto` (
  `idreparto` int(11) NOT NULL,
  `descripcion` varchar(45) DEFAULT NULL,
  `actor` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `reseña`
--

CREATE TABLE `reseña` (
  `idreseña` int(11) NOT NULL,
  `texto_reseña` varchar(45) NOT NULL,
  `puntajeEstrellas` int(11) NOT NULL,
  `idCritico_fk` int(11) NOT NULL,
  `idPelicula_fk` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `actor`
--
ALTER TABLE `actor`
  ADD PRIMARY KEY (`idactor`);

--
-- Indices de la tabla `administrador`
--
ALTER TABLE `administrador`
  ADD PRIMARY KEY (`idadministrador`);

--
-- Indices de la tabla `carrito`
--
ALTER TABLE `carrito`
  ADD PRIMARY KEY (`idCarrito`);

--
-- Indices de la tabla `carrito_pelicula`
--
ALTER TABLE `carrito_pelicula`
  ADD PRIMARY KEY (`idCarrito_pelicula`);

--
-- Indices de la tabla `cliente`
--
ALTER TABLE `cliente`
  ADD PRIMARY KEY (`IdCliente`);

--
-- Indices de la tabla `critico`
--
ALTER TABLE `critico`
  ADD PRIMARY KEY (`idcritico`);

--
-- Indices de la tabla `director`
--
ALTER TABLE `director`
  ADD PRIMARY KEY (`idDirector`);

--
-- Indices de la tabla `funcion`
--
ALTER TABLE `funcion`
  ADD PRIMARY KEY (`idfuncion`),
  ADD KEY `fk_funcion_pelicula1` (`pelicula`),
  ADD KEY `fk_funcion_horario1` (`horario`);

--
-- Indices de la tabla `genero`
--
ALTER TABLE `genero`
  ADD PRIMARY KEY (`idgenero`);

--
-- Indices de la tabla `horario`
--
ALTER TABLE `horario`
  ADD PRIMARY KEY (`idhorario`);

--
-- Indices de la tabla `idioma`
--
ALTER TABLE `idioma`
  ADD PRIMARY KEY (`ididioma`);

--
-- Indices de la tabla `logadministrador`
--
ALTER TABLE `logadministrador`
  ADD PRIMARY KEY (`idLogadministrador`);

--
-- Indices de la tabla `logcliente`
--
ALTER TABLE `logcliente`
  ADD PRIMARY KEY (`idLogcliente`);

--
-- Indices de la tabla `logcritico`
--
ALTER TABLE `logcritico`
  ADD PRIMARY KEY (`idLogcritico`);

--
-- Indices de la tabla `pelicula`
--
ALTER TABLE `pelicula`
  ADD PRIMARY KEY (`idpelicula`),
  ADD KEY `fk_pelicula_idioma` (`idioma`),
  ADD KEY `fk_pelicula_genero1` (`genero`),
  ADD KEY `fk_pelicula_director1` (`idDirector`);

--
-- Indices de la tabla `reparto`
--
ALTER TABLE `reparto`
  ADD PRIMARY KEY (`idreparto`),
  ADD KEY `fk_reparto_actor1` (`actor`);

--
-- Indices de la tabla `reseña`
--
ALTER TABLE `reseña`
  ADD PRIMARY KEY (`idreseña`),
  ADD KEY `fk_reseña_critico_idx` (`idCritico_fk`),
  ADD KEY `fk_reseña_pelicula1_idx` (`idPelicula_fk`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `actor`
--
ALTER TABLE `actor`
  MODIFY `idactor` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `administrador`
--
ALTER TABLE `administrador`
  MODIFY `idadministrador` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `carrito`
--
ALTER TABLE `carrito`
  MODIFY `idCarrito` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `carrito_pelicula`
--
ALTER TABLE `carrito_pelicula`
  MODIFY `idCarrito_pelicula` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `cliente`
--
ALTER TABLE `cliente`
  MODIFY `IdCliente` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `critico`
--
ALTER TABLE `critico`
  MODIFY `idcritico` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `director`
--
ALTER TABLE `director`
  MODIFY `idDirector` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `funcion`
--
ALTER TABLE `funcion`
  MODIFY `idfuncion` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `genero`
--
ALTER TABLE `genero`
  MODIFY `idgenero` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de la tabla `horario`
--
ALTER TABLE `horario`
  MODIFY `idhorario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `idioma`
--
ALTER TABLE `idioma`
  MODIFY `ididioma` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `logadministrador`
--
ALTER TABLE `logadministrador`
  MODIFY `idLogadministrador` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT de la tabla `logcliente`
--
ALTER TABLE `logcliente`
  MODIFY `idLogcliente` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `logcritico`
--
ALTER TABLE `logcritico`
  MODIFY `idLogcritico` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `pelicula`
--
ALTER TABLE `pelicula`
  MODIFY `idpelicula` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT de la tabla `reparto`
--
ALTER TABLE `reparto`
  MODIFY `idreparto` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `reseña`
--
ALTER TABLE `reseña`
  MODIFY `idreseña` int(11) NOT NULL AUTO_INCREMENT;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `carrito`
--
ALTER TABLE `carrito`
  ADD CONSTRAINT `carrito_ibfk_1` FOREIGN KEY (`idCarrito`) REFERENCES `cliente` (`IdCliente`);

--
-- Filtros para la tabla `carrito_pelicula`
--
ALTER TABLE `carrito_pelicula`
  ADD CONSTRAINT `carrito_pelicula_ibfk_1` FOREIGN KEY (`idCarrito_pelicula`) REFERENCES `pelicula` (`idpelicula`);

--
-- Filtros para la tabla `funcion`
--
ALTER TABLE `funcion`
  ADD CONSTRAINT `fk_funcion_horario1` FOREIGN KEY (`horario`) REFERENCES `horario` (`idhorario`),
  ADD CONSTRAINT `fk_funcion_pelicula1` FOREIGN KEY (`pelicula`) REFERENCES `pelicula` (`idpelicula`);

--
-- Filtros para la tabla `logadministrador`
--
ALTER TABLE `logadministrador`
  ADD CONSTRAINT `logadministrador_ibfk_1` FOREIGN KEY (`idLogadministrador`) REFERENCES `administrador` (`idadministrador`);

--
-- Filtros para la tabla `logcliente`
--
ALTER TABLE `logcliente`
  ADD CONSTRAINT `logcliente_ibfk_1` FOREIGN KEY (`idLogcliente`) REFERENCES `cliente` (`IdCliente`);

--
-- Filtros para la tabla `logcritico`
--
ALTER TABLE `logcritico`
  ADD CONSTRAINT `logcritico_ibfk_1` FOREIGN KEY (`idLogcritico`) REFERENCES `critico` (`idcritico`);

--
-- Filtros para la tabla `pelicula`
--
ALTER TABLE `pelicula`
  ADD CONSTRAINT `fk_pelicula_director1` FOREIGN KEY (`idDirector`) REFERENCES `director` (`idDirector`),
  ADD CONSTRAINT `fk_pelicula_genero1` FOREIGN KEY (`genero`) REFERENCES `genero` (`idgenero`),
  ADD CONSTRAINT `fk_pelicula_idioma` FOREIGN KEY (`idioma`) REFERENCES `idioma` (`ididioma`);

--
-- Filtros para la tabla `reparto`
--
ALTER TABLE `reparto`
  ADD CONSTRAINT `fk_reparto_actor1` FOREIGN KEY (`actor`) REFERENCES `actor` (`idactor`);

--
-- Filtros para la tabla `reseña`
--
ALTER TABLE `reseña`
  ADD CONSTRAINT `fk_reseña_critico` FOREIGN KEY (`idCritico_fk`) REFERENCES `mydb`.`critico` (`idcritico`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_reseña_pelicula1` FOREIGN KEY (`idPelicula_fk`) REFERENCES `pelicula` (`idpelicula`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
