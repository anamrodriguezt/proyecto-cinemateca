<?php
require_once "persistencia/Conexion.php";
require_once "persistencia/LogclienteDAO.php";
class LogCliente{
    private $idLogcliente;
    private $Accion;
    private $Fecha;
    private $Hora;
    private $conexion;
    private $LogaclienteDAO;
    private $idcliente;
    
    public function getIdLogCliente(){
        return $this -> idLogliente;
    }
    
    
    public function getAccion(){
        return $this -> Accion;
    }
    
    public function getFecha(){
        return $this -> Fecha;
    }
    
    public function getHora(){
        return $this -> Hora;
    }
     public function getidCliente(){
        return $this -> idCliente;
    }
 
    
    public function LogCliente($idLogcliente = "", $Accion = "", $Fecha = "", $Hora = "", $idCliente=""){
        $this -> idLogcliente = $idLogcliente;
        $this -> Accion = $Accion;
        $this -> Fecha = $Fecha;
        $this -> Hora = $Hora;
        $this -> idCliente = $idCliente;
        $this -> conexion = new Conexion();
        $this -> LogclienteDAO = new LogclienteDAO($this -> idLogcliente ,$this -> Accion ,$this -> Fecha,$this -> Hora,$this -> idCliente);
    }
    
 

    
    public function insertar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> LogclienteDAO -> insertar());
        $this -> conexion -> cerrar();
    }
    public function consultar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> LogclienteDAO -> consultar());
        $this -> conexion -> cerrar();
    }
    public function consultarFiltro($filtro){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> LogclienteDAO -> consultarFiltro($filtro));
        $Logcliente = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $l = new Logcliente($resultado[0], $resultado[1], $resultado[2], $resultado[3],$resultado[4]);
            array_push($Logcliente, $l);
        }
        $this -> conexion -> cerrar();
        return $LogCliente;
    }
  
    public function consultarCantidad(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> LogclienteDAO -> consultarCantidad());
        $this -> conexion -> cerrar();
        return $this -> conexion -> extraer()[0];
    }
}

?>