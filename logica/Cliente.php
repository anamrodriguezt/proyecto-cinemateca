<?php
require "persistencia/ClienteDAO.php";


class Cliente extends Persona{
    
    private $estado;
    private $foto;
    private $conexion;
    private $clienteDAO;    
    

    public function getEstado()
    {
        return $this->estado;
    }

    public function getFoto()
    {
        return $this->foto;
    }
    
    function Cliente ($id="", $nombre="", $apellido="", $correo="", $clave="", $pEstado="", $pFoto="") {
        $this -> Persona($id, $nombre, $apellido, $correo, $clave);
        $this -> estado = $pEstado;
        $this -> foto = $pFoto;
        $this -> conexion = new Conexion();
        $this -> clienteDAO = new ClienteDAO($id, $nombre, $apellido, $correo, $clave, $pEstado, $pFoto);        
    }
    
    function autenticar () {
        $this -> conexion -> abrir();        
        $this -> conexion -> ejecutar($this -> clienteDAO -> autenticar());
        $this -> conexion -> cerrar();
        if($this -> conexion -> numFilas() == 1){
            $resultado = $this -> conexion -> extraer();
            $this -> id = $resultado[0];
            $this -> estado = $resultado[1];
            return true;
        }else{
            return false;
        }
    }
    
    function consultar(){
        $this -> conexion -> abrir();     
        $this -> conexion -> ejecutar($this -> clienteDAO -> consultar());
        $resultado = $this -> conexion -> extraer();
        $this -> nombre = $resultado[0];
        $this -> apellido = $resultado[1];
        $this -> correo = $resultado[2];    
        $this -> estado = $resultado[3];
        $this -> foto = $resultado[4];
        $this -> conexion -> cerrar();        

    }
    
    function consultarTodos(){
        $this -> conexion -> abrir();        
        $this -> conexion -> ejecutar($this -> clienteDAO -> consultarTodos());
        $this -> conexion -> cerrar();
        $clientes = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            array_push($clientes, new Cliente($resultado[0], $resultado[1], $resultado[2], $resultado[3], "", $resultado[4], $resultado[5]));
        }
        return $clientes;
    }
    

        public function consultarFiltro($filtro){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> clienteDAO -> consultarFiltro($filtro));
        $clientes = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $c = new Cliente($$resultado[0], $resultado[1], $resultado[2], $resultado[3], "", $resultado[4], $resultado[5]);
            array_push($clientes, $c);
        }
        $this -> conexion -> cerrar();
        return $clientes;
    }

    function cambiarEstado($estado){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> clienteDAO -> cambiarEstado($estado));
        $this -> conexion -> cerrar();
    }
    
    function editarFoto(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> clienteDAO -> editarFoto());
        $this -> conexion -> cerrar();
    }
    
}


?>