<?php
require 'persistencia/CriticoDAO.php';


class Critico extends Persona{
    private $estado;
    private $foto;
    private $criticoDAO;
    private $conexion;
    
    public function getEstado()
    {
        return $this->estado;
    }

    public function getFoto()
    {
        return $this->foto;
    }
    function Critico($id="", $nombre="", $apellido="", $correo="", $clave="", $pEstado="", $pFoto=""){
        $this -> Persona($id, $nombre, $apellido, $correo, $clave);           
        $this -> conexion = new Conexion();
        $this -> criticoDAO = new CriticoDAO($id, $nombre, $apellido, $correo, $clave, $pEstado, $pFoto);        
    }
    function autenticar () {
        $this -> conexion -> abrir();        
        $this -> conexion -> ejecutar($this -> criticoDAO -> autenticar());
        echo $this -> criticoDAO -> autenticar();
        $this -> conexion -> cerrar();
        if($this -> conexion -> numFilas() == 1){
            $resultado = $this -> conexion -> extraer();
            $this -> id = $resultado[0];
            $this -> estado = $resultado[1];
            return true;
        }else{
            return false;
        }
    }
    
    
    
    function consultar(){
        $this -> conexion -> abrir();     
        $this -> conexion -> ejecutar($this -> criticoDAO -> consultar());
        $resultado = $this -> conexion -> extraer();
        $this -> nombre = $resultado[0];
        $this -> apellido = $resultado[1];
        $this -> correo = $resultado[2];    
        $this -> estado = $resultado[3];
        $this -> foto = $resultado[4];
        $this -> conexion -> cerrar();        

    }

    function consultarTodos(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> criticoDAO -> consultarTodos());
        $resultados = array();
        $i=0;
        while(($registro = $this -> conexion -> extraer()) != null){
            $resultados[$i] = new Critico($registro[0], $registro[1], $registro[2]);
            $i++;
        }
        $this -> conexion -> cerrar();
        return $resultados;
    }
    
    
}