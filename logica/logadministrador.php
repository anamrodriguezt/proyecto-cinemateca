<?php
require_once "persistencia/Conexion.php";
require_once "persistencia/LogadministradorDAO.php";
class logadministrador{
    private $idLogadministrador;
    private $Accion;
    private $Fecha;
    private $Hora;
    private $conexion;
    private $LogadministradorDAO;
    private $idAdministrador;
    
    public function getIdLogadministrador(){
        return $this -> idLogadministrador;
    }
    
    
    public function getAccion(){
        return $this -> Accion;
    }
    
    public function getFecha(){
        return $this -> Fecha;
    }
    
    public function getHora(){
        return $this -> Hora;
    }
     public function getidAdministrador(){
        return $this -> idAdministrador;
    }
 
    
    public function logadministrador($idLogadministrador = "", $Accion = "", $Fecha = "", $Hora = "", $idAdministrador=""){
        $this -> idLogadministrador = $idLogadministrador;
        $this -> Accion = $Accion;
        $this -> Fecha = $Fecha;
        $this -> Hora = $Hora;
        $this -> idAdministrador = $idAdministrador;
        $this -> conexion = new Conexion();
        $this -> LogadministradorDAO = new LogadministradorDAO($this -> idLogadministrador ,$this -> Accion ,$this -> Fecha,$this -> Hora,$this -> idAdministrador);
    }
    
 

    
    public function insertar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> LogadministradorDAO -> insertar());
        $this -> conexion -> cerrar();
    }
    public function consultar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> LogadministradorDAO -> consultar());
        $this -> conexion -> cerrar();
    }
    public function consultarFiltro($filtro){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> LogadministradorDAO -> consultarFiltro($filtro));
        $Logadministrador = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $l = new Logadministrador($resultado[0], $resultado[1], $resultado[2], $resultado[3]);
            array_push($Logadministrador, $l);
        }
        $this -> conexion -> cerrar();
        return $Logadministrador;
    }
    public function consultarPaginacion($cantidad, $pagina){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> LogadministradorDAO -> consultarPaginacion($cantidad, $pagina));
        $Logadministrador= array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $l = new Logadministrador($resultado[0], $resultado[1], $resultado[2], $resultado[3]);
            array_push($Logadministrador, $l);
        }
        $this -> conexion -> cerrar();
        return $Logadministrador;
    }
    public function consultarCantidad(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> LogadministradorDAO -> consultarCantidad());
        $this -> conexion -> cerrar();
        return $this -> conexion -> extraer()[0];
    }
}

?>