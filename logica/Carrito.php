<?php
require_once "persistencia/Conexion.php";
require_once "persistencia/CarritoDAO.php";
class Carrito{
    private $idCarrito;
    private $fecha;
    private $valorFinal;
    private $idCliente;    
    private $conexion;
    private $carritoaDAO;
  
    public function getIdCarrito()
    {
        return $this->idCarrito;
    }

    public function getFecha()
    {
        return $this->fecha;
    }

    public function getIdCliente()
    {
        return $this->idCliente;
    }
    public function setValorFinal($valor){
        $this->valorFinal=$valor;
        $this->carritoDAO->setValorFinal($valor);
    }
    public function getValorFinal(){
       return $this->valorFinal;
    }


    public function Carrito($idCarrito = "", $Fecha = "",$valorFinal = "", $idCliente = ""){
        $this -> idCarrito = $idCarrito;
        $this -> fecha = $Fecha;
        $this -> valorFinal=$valorFinal;
        $this -> idCliente = $idCliente;
        $this -> conexion = new Conexion();
        $this -> carritoDAO = new CarritoDAO($this -> idCarrito, $this -> fecha, $this -> valorFinal,$this -> idCliente);
    }
    
    public function consultar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> carritoDAO -> consultar());
        echo $this -> carritoDAO -> consultar();
        $this -> conexion -> cerrar();
        $resultado = $this -> conexion -> extraer();
        $this -> fecha = $resultado[0];
        $this -> valorFinal = $resultado[1];
        $this -> idCliente = $resultado[2];
    }
    public function consultarUltimo(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> carritoDAO -> consultarUltimo());
        echo $this -> carritoDAO -> consultarUltimo();
        $this -> conexion -> cerrar();
        $resultado = $this -> conexion -> extraer();
        $this -> fecha = $resultado[0];
        $this -> valorFinal = $resultado[1];
        $this -> idCliente = $resultado[2];
    }
    
    public function insertar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> carritoDAO -> insertar());
        $this -> conexion -> cerrar();
    }
    public function actualizar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> carritoDAO -> actualizar());
        $this -> conexion -> cerrar();
    }
    
    public function consultarTodos(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> carritoDAO -> consultarTodos());
        $clientes = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $c = new Carrito($resultado[0], $resultado[1], $resultado[2]);
            array_push($clientes, $c);
        }
        $this -> conexion -> cerrar();
        return $clientes;
    }
    public function consultarPeliculasEnCarrito(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> carrito_peliculaDAO -> consultarPeliculasEnCarrito());
        $carrito_peliculas = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $pelicula = $resultado;
            array_push($carrito_peliculas, $pelicula);
        }
        $this -> conexion -> cerrar();
        return $carrito_peliculas;

    }
    
    //     public function consultarPaginacion($cantidad, $pagina){
    //         $this -> conexion -> abrir();
    //         $this -> conexion -> ejecutar($this -> carrito_peliculaDAO -> consultarPaginacion($cantidad, $pagina));
    //         $productos = array();
    //         while(($resultado = $this -> conexion -> extraer()) != null){
    //             $p = new Producto($resultado[0], $resultado[1], $resultado[2], $resultado[3]);
    //             array_push($productos, $p);
    //         }
    //         $this -> conexion -> cerrar();
    //         return $productos;
    //     }
    
    //     public function consultarCantidad(){
    //         $this -> conexion -> abrir();
    //         $this -> conexion -> ejecutar($this -> carrito_peliculaDAO -> consultarCantidad());
    //         $this -> conexion -> cerrar();
    //         return $this -> conexion -> extraer()[0];
    //     }
    
    //     public function editar(){
    //         $this -> conexion -> abrir();
    //         $this -> conexion -> ejecutar($this -> carrito_peliculaDAO -> editar());
    //         $this -> conexion -> cerrar();
    //     }
    
}

?>