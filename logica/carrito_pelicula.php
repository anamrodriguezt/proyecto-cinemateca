<?php
require_once "persistencia/Conexion.php";
require_once "persistencia/carrito_peliculaDAO.php";
class carrito_pelicula{
    private $idCarrito_pelicula;
    private $cantidad;
    private $precio;
    private $idPelicula;
    private $idCarrito;
    private $conexion;
    private $idPeliculaAux;
    private $cantidadAux;
    private $arrayid;
    private $arraycant;
    private $carrito_peliculaDAO;

    
   
    public function getArrayid()
    {
        return $this->arrayid;
    }

    public function getArraycant()
    {
        return $this->arraycant;
    }

    public function setCantidad($cantidad)
    {
        $this->cantidad = $cantidad;
    }

   
    public function setIdPelicula($idPelicula)
    {
        $this->idPelicula = $idPelicula;
    }

    public function getIdCarrito_pelicula()
    {
        return $this->idCarrito_pelicula;
    }

    public function getCantidad()
    {
        return $this->cantidad;
    }

    public function getPrecio()
    {
        return $this->precio;
    }

    public function getIdPelicula()
    {
        return $this->idPelicula;
    }

    public function getIdCarrito()
    {
        return $this->idCarrito;
    }
   
     
   

    public function PeliculaCarrito($idCarrito_pelicula = "", $cantidad = "", $precio = "", $idPelicula = "", $idCarrito = ""){
        $this -> idCarrito_pelicula = $idCarrito_pelicula;
        $this -> cantidad = $cantidad;
        $this -> precio = $precio*$cantidad;
        $this -> idPelicula = $idPelicula;
        $this -> idCarrito = $idCarrito;
        $this -> arrayid = new ArrayObject();
        $this -> arraycant = new ArrayObject();
        $this -> conexion = new Conexion();
        $this -> carrito_peliculaDAO = new carrito_peliculaDAO($this -> idProducto, $this -> cantidad, $this -> precio,$this -> idPelicula, $this -> idCarrito);
    }
    
    public function consultar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> carrito_peliculaDAO -> consultar());
        $this -> conexion -> cerrar();
        $resultado = $this -> conexion -> extraer();
        $this -> nombre = $resultado[0];
        $this -> cantidad = $resultado[1];
        $this -> precio = $resultado[2];
        $this -> imagen = $resultado[3];
    }
    
    public function insertar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> carrito_peliculaDAO -> insertar());
        $this -> conexion -> cerrar();
    }
    
    public function consultarTodos(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> carrito_peliculaDAO -> consultarTodos());
        $peliculas = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $p = new Pelicula($resultado[0], $resultado[1], $resultado[2], $resultado[3],$resultado[4]);
            array_push($peliculas, $p);
        }
        $this -> conexion -> cerrar();
        return $peliculas;
    }
    
//     public function consultarPaginacion($cantidad, $pagina){
//         $this -> conexion -> abrir();
//         $this -> conexion -> ejecutar($this -> carrito_peliculaDAO -> consultarPaginacion($cantidad, $pagina));
//         $peliculas = array();
//         while(($resultado = $this -> conexion -> extraer()) != null){
//             $p = new Pelicula($resultado[0], $resultado[1], $resultado[2], $resultado[3],$resultado[4]);
//             array_push($peliculas, $p);
//         }
//         $this -> conexion -> cerrar();
//         return $peliculas;
//     }
    
//     public function consultarCantidad(){
//         $this -> conexion -> abrir();
//         $this -> conexion -> ejecutar($this -> carrito_peliculaDAO -> consultarCantidad());
//         $this -> conexion -> cerrar();
//         return $this -> conexion -> extraer()[0];
//     }
    
//     public function editar(){
//         $this -> conexion -> abrir();
//         $this -> conexion -> ejecutar($this -> carrito_peliculaDAO -> editar());
//         $this -> conexion -> cerrar();
//     }
    public function agregarArray(){
        array_push($this -> arrayid,$this -> idPelicula);
        array_push($this -> arraycant,$this -> cantidad);
        
    }
}

?>