<?php
require 'persistencia/ReseñaDAO.php';
require_once 'persistencia/Conexion.php';

class Reseña{

    private $id;
    private $texto_reseña;
    private $puntajeEstrellas;
    private $ReseñaDAO;
    private $conexion;
 
    function getId(){
        return $this -> id;
    }
    
    function gettexto_reseña(){
        return $this -> texto_reseña;
    }
    
    function getApellido(){
        return $this -> apellido;
    }
    
    
    function Critico($id="", $texto_reseña="", $apellido=""){
        $this -> id = $id;
        $this -> texto_reseña = $texto_reseña;
        $this -> apellido = $apellido;               
        $this -> conexion = new Conexion();
        $this -> CriticoDAO = new CriticoDAO($id, $texto_reseña, $apellido);
    }
    
    function consultar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> CriticoDAO -> consultar());
        $resultado = $this -> conexion -> extraer();
        $this -> texto_reseña = $texto_reseña[0];
        $this -> apellido = $resultado[1];
        $this -> conexion -> cerrar();
    }

    function consultarTodos(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> CriticoDAO -> consultarTodos());
        $resultados = array();
        $i=0;
        while(($registro = $this -> conexion -> extraer()) != null){
            $resultados[$i] = new Director($registro[0], $registro[1], $registro[2]);
            $i++;
        }
        $this -> conexion -> cerrar();
        return $resultados;
    }
    
    
}