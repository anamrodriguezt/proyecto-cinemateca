<?php
require_once "persistencia/Conexion.php";
require_once "persistencia/LogcriticoDAO.php";
class LogCritico{
    private $idLogcritico;
    private $Accion;
    private $Fecha;
    private $Hora;
    private $conexion;
    private $LogacriticoDAO;
    private $idcritico;
    
    public function getIdLogCritico(){
        return $this -> idLogCritico;
    }
    
    
    public function getAccion(){
        return $this -> Accion;
    }
    
    public function getFecha(){
        return $this -> Fecha;
    }
    
    public function getHora(){
        return $this -> Hora;
    }
     public function getidCritico(){
        return $this -> idcritico;
    }
 
    
    public function LogCritico($idLogCritico = "", $Accion = "", $Fecha = "", $Hora = "", $idcritico=""){
        $this -> idLogcritico = $idLogcritico;
        $this -> Accion = $Accion;
        $this -> Fecha = $Fecha;
        $this -> Hora = $Hora;
        $this -> idCritico = $idCritico;
        $this -> conexion = new Conexion();
        $this -> LogcriticoDAO = new LogcriticoDAO($this -> idLogcritico ,$this -> Accion ,$this -> Fecha,$this -> Hora,$this -> idCritico);
    }
    
 

    
    public function insertar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> LogcriticoDAO -> insertar());
        $this -> conexion -> cerrar();
    }
    public function consultar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> LogcriticoDAO -> consultar());
        $this -> conexion -> cerrar();
    }
    public function consultarFiltro($filtro){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> LogcriticoDAO -> consultarFiltro($filtro));
        $LogCritico = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $l = new LogCritico($resultado[0], $resultado[1], $resultado[2], $resultado[3],$resultado[4]);
            array_push($LogCritico, $l);
        }
        $this -> conexion -> cerrar();
        return $LogCritico;
    }
  
    public function consultarCantidad(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> LogCriticoDAO -> consultarCantidad());
        $this -> conexion -> cerrar();
        return $this -> conexion -> extraer()[0];
    }
}

?>