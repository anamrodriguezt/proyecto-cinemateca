<?php
class LogadministradorDAO{
    private $idLogadministrador;
    private $Accion;
    private $Fecha;
    private $Hora;
    private $conexion;
    private $LogadministradorDAO;
    private $idAdministrador;
    
  
    
    public function LogadministradorDAO($idLogadministrador = "", $Accion = "", $Fecha = "", $Hora = "", $idAdministrador=""){
        $this -> idLogadministrador = $idLogadministrador;
        $this -> Accion = $Accion;
        $this -> Fecha = $Fecha;
        $this -> Hora = $Hora;
        $this -> conexion = new Conexion();
    }
    
    
    
    
    public function insertar(){
        return "insert into Logadministrador(accion,fecha,hora,idAdministrador)
                values ('". $this -> Accion ."','". $this -> Fecha ."','". $this -> Hora ."','". $this -> idAdministrador."')";
        
    }
    public function consultar(){
        return "Select id,accion,fecha,hora,idAdministrador
                From Logadministrador";
        
    }
    public function consultarFiltro($filtro){
        return "select  id,accion,fecha,hora,idAdministrador
                from Logadministrador
                where autor like '%" . $filtro . "%' or accion like '%" . $filtro . "%'";
    }
    public function consultarPaginacion($cantidad, $pagina){
        return "select id,accion,fecha,hora,idAdministrador
                from Logadministrador
                limit " . (($pagina-1) * $cantidad) . ", " . $cantidad;
    }
    public function consultarCantidad(){
        return "select count(id)
                from Logadministrador";
    }
}