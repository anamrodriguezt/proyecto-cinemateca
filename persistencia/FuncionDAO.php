<?php
require_once 'persistencia/Conexion.php';

class FuncionDAO{
    
    private $id;
    private $fechaInicio;
    private $fechaFin;
    private $pelicula;
    private $horario;
    private $conexion;
       
    function FuncionDAO($id="", $fechaInicio="", $fechaFin="", $pelicula="", $horario=""){
        $this -> id = $id;
        $this -> fechaInicio = $fechaInicio;
        $this -> fechaFin = $fechaFin;
        $this -> pelicula = $pelicula;
        $this -> horario = $horario;
    }
    function consultarTodos(){
        return "select *
                from funcion";
    }
    function consultarDia(){
        return "select *
                from funcion
                where fechaInicio<=curdate() and fechaFin>=curdate() ";
    }
    
    function consultar(){
        return "select f.fechaInicio, f.fechaFin, f.pelicula, h.horarioInicio
                from funcion AS f, horario AS h
                where idfuncion = '" . $this -> id . "'";
    }
    function actualizar(){
        return "update funcion set
             fechaInicio = '" . $this -> fechaInicio . "',
             fechaFin='" . $this -> fechaFin . "',
             Pelicula ='" . $this -> pelicula . "',
             horario='" . $this -> horario . "'
             where idfuncion='" . $this -> id ."'";
    }
    function insertar(){
        return "insert into funcion
                (fechaInicio, fechaFin, pelicula, horario)
                values ('" . $this -> fechaInicio . "' ,
                     '" . $this -> fechaFin . "',
                     '" . $this -> pelicula . "',
                     '" . $this -> horario . "')";
    }
}
// select f.horario, h.idhorario, h.horainicio 
    //from funcion AS f, horario AS h
// where f.horario = h.idhorario;