<?php
class PeliculaDAO {
    private $id;
    private $nombre;
    private $sinopsis;
    private $idioma;
    private $imagen;
    private $genero;
    private $precio;
    private $cantidad;
    private $idDirector;
    
    function PeliculaDAO ($id="", $nombre="", $sinopsis="", $idioma="", $imagen="", $genero="",$precio="",$cantidad="",$idDirector=""){
        $this -> id = $id;
        $this -> nombre = $nombre;
        $this -> sinopsis = $sinopsis;
        $this -> idioma = $idioma;
        $this -> imagen = $imagen;
        $this -> genero = $genero;
         $this -> precio = $precio;
        $this -> cantidad = $cantidad;
        $this -> idDirector = $idDirector;
    }
    
    function insertar(){
        return "insert into pelicula
                (nombre, sinopsis, idioma, genero, precio, cantidad, idDirector)
                values ('" . $this -> nombre . "' , 
                     '" . $this -> sinopsis . "',
                     '" . $this -> idioma . "',
                     '" . $this -> genero . "',
                     '" . $this -> precio . "',
                      '" . $this -> cantidad . "',
                     '" . $this -> idDirector . "')";
    }
    
    function actualizar(){

    
       
 return "update pelicula set nombre='".$this->nombre."', sinopsis='".$this->sinopsis."', idioma='".$this->idioma."', imagen='".$this->imagen."', genero='".$this->genero."', cantidad='".$this->cantidad."' , precio='".$this->precio."', idDirector='".$this->idDirector."' where idpelicula='".$this->id."' ";
    
    }
    
    function actualizarFoto(){
        return "update pelicula set
                imagen = '" . $this -> imagen . "'
                where idpelicula='" . $this -> id . "'";
    }
    
    function consultar(){
        return "select  nombre, sinopsis, idioma, imagen, genero ,precio,cantidad,idDirector
                from pelicula
                where idpelicula = '" . $this -> id . "'";
    }

   public function consultarCantidad(){
        return "select count(idpelicula)
                from pelicula";
    }

    
    function consultarTodos(){
        return "select idpelicula, nombre, sinopsis, idioma, imagen, genero,precio,cantidad, idDirector
                from pelicula
                order by nombre";
    }
}


?>