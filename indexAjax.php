<script type="text/javascript">
$(function () {
	  $('[data-toggle="tooltip"]').tooltip()
	})
</script>
<?php
session_start();
require "logica/Administrador.php";
require "logica/Persona.php";
require "logica/Cliente.php";
require "logica/Critico.php";
require "logica/PeliculaCarrito.php";
require "logica/Carrito.php";
require "logica/LogAdministrador.php";
require "logica/LogCliente.php";
require "logica/LogCritico.php";

if(isset($_SESSION["id"])){
    $pid = base64_decode($_GET["pid"]);
    include $pid;    
}else{
    header("Location: index.php");
}
?>