<?php
$filtro = $_GET["filtro"];
$pelicula = new Pelicula();
$peliculas = $Pelicula -> consultarFiltro($filtro);
?>
<div class="container mt-3">
	<div class="row">
		<div class="col">
            <div class="card">
				<div class="card-header text-black bg-warning">
					<h4>Consultar Pelicula</h4>
				</div>
				<div class="text-right"><?php echo count($peliculas) ?> registros encontrados</div>
              	<div class="card-body">
					<table class="table table-hover table-striped">
						<tr>
							<th>#</th>
							<th>Nombre</th>
							<th>Cantidad</th>
							<th>Precio</th>
							<th></th>
						</tr>
						<?php 
						$i=1;
						foreach($peliculas as $productoActual){
// 						    $posiciones = array();
// 						    for($i=0; $i<strlen($productoActual -> getNombre())-strlen($filtro)+1; $i++){
// 						        if(strtolower(substr($productoActual -> getNombre(), $i, strlen($filtro))) == strtolower($filtro)){
// 						            array_push($posiciones, $i);
// 						        }
// 						    }
						    $pos = stripos($peliculaActual -> getNombre(), $filtro);
						    echo "<tr>";
						    echo "<td>" . $i . "</td>";
						    if($pos === false){
						        echo "<td>" . $peliculaActual -> getNombre() . "</td>";						        
						    }else{						        
						        echo "<td>" . substr($peliculaActual -> getNombre(), 0, $pos) . "<mark>" . substr($peliculaActual -> getNombre(), $pos, strlen($filtro)) . "</mark>" . substr($peliculaActual -> getNombre(), $pos+strlen($filtro)) . "</td>";
						    }
						    echo "<td>" . $peliculaActual -> getCantidad() . "</td>";
						    echo "<td>" . $peliculaActual -> getPrecio() . "</td>";
						    echo "<td><a href='index.php?pid=". base64_encode("presentacion/producto/editarPelicula.php") . "&idPelicula=" . $peliculaActual -> getIdPelicula(). "' data-toggle='tooltip' data-placement='left' title='Editar'><span class='fas fa-edit'></span></a></td>";
						    echo "</tr>";
						    $i++;
						}
						?>
					</table>
				</div>
            </div>
		</div>
	</div>
</div>