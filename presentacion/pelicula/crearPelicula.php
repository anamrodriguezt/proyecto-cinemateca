<?php
$administrador= new Administrador($_SESSION["id"]);
$nombre = "";
if(isset($_POST["nombre"])){
    $nombre = $_POST["nombre"];
}
$cantidad = "";
if(isset($_POST["cantidad"])){
    $cantidad = $_POST["cantidad"];
}
$precio = "";
if(isset($_POST["precio"])){
    $precio = $_POST["precio"];
}

if(isset($_POST["crear"])){
    if($_FILES["imagen"]["name"] != ""){
        $rutaLocal = $_FILES["imagen"]["tmp_name"];
        $tipo = $_FILES["imagen"]["type"];
        $tiempo = new DateTime();
        $rutaRemota = "img/" . $tiempo -> getTimestamp() . (($tipo == "image/png")?".png":".jpg");
        copy($rutaLocal,$rutaRemota);
        
        $Pelicula = new Pelicula("",$nombre, $cantidad, $precio, $rutaRemota);
        $Pelicula -> insertar();
    }else{
        $Pelicula = new Pelicula("",$nombre, $cantidad, $precio);
        $Pelicula -> insertar();
    }
    $administrador -> consultar();
    $actor=$_SESSION["rol"].": ".$administrador -> getNombre()." ". $administrador -> getApellido();
    $datos= $nombre;
    $log =new Log("",$actor,"Creacion de Pelicula",$datos,date("Y-m-d"),date("H:i:s"));
    $log -> insertar();
}
?>
<div class="container mt-3">
    <div class="row">
        <div class="col-lg-3 col-md-0"></div>
        <div class="col-lg-6 col-md-12">
            <div class="card">
                <div class="card-header text-black bg-warning">
                    <h4>Crear Pelicula</h4>
                </div>
                <div class="card-body">
                    <?php if(isset($_POST["crear"])){ ?>
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        Datos creados
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    </div>
                    <?php } ?>
                    <form action="index.php?pid=<?php echo base64_encode("presentacion/pelicula/crearPelicula.php") ?>" method="post" enctype="multipart/form-data">
                        <div class="form-group">
                            <label>Nombre</label> 
                            <input type="text" name="nombre" class="form-control" value="<?php echo $nombre ?>" required>
                        </div>
                        <div class="form-group">
                            <label>Cantidad</label> 
                            <input type="number" name="cantidad" class="form-control" min="1" value="<?php echo $cantidad ?>" required>
                        </div>
                        <div class="form-group">
                            <label>Precio</label> 
                            <input type="number" name="precio" class="form-control" min="1" value="<?php echo $precio ?>" required>
                        </div>
                        <div class="form-group">
                            <label>Imagen</label> 
                            <input type="file" name="imagen" class="form-control" >
                        </div>
                        <button type="submit" name="crear" class="btn btn-warning">Crear</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>