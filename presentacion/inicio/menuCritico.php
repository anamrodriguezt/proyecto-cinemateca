<?php
$critico = new Critico($_SESSION["id"]);
$critico -> consultar();
$items = 1;
if($critico -> getNombre() != ""){
    $items++;
}
if($critico -> getApellido() != ""){
    $items++;
}
if($critico -> getFoto() != ""){
    $items++;
}
$porcentaje = $items/4 * 100;
?>
<nav class="navbar navbar-expand-md navbar-light bg-warning">
  <a class="navbar-brand" href="index.php?pid=<?php echo base64_encode("presentacion/inicio/sesionCritico.php") ?>"><i class="fas fa-home"></i></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse"
    data-target="#navbarSupportedContent"
    aria-controls="navbarSupportedContent" aria-expanded="false"
    aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item dropdown active"><a class="nav-link dropdown-toggle"
        href="#" id="navbarDropdown" role="button" data-toggle="dropdown"
        aria-haspopup="true" aria-expanded="false">Reseña de pelicula</a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/critico/crearreseña.php")?>">Crea una reseña</a>
    
        </div></li>

    </ul>
    <ul class="navbar-nav">
      <li class="nav-item dropdown active"><a class="nav-link dropdown-toggle"
        href="#" id="navbarDropdown" role="button" data-toggle="dropdown"
        aria-haspopup="true" aria-expanded="false"><span class="badge badge-danger"><?php echo $porcentaje . "%"?></span>
        Critico: <?php echo ($critico -> getNombre()!=""?$critico -> getNombre():$critico -> getCorreo()) ?> <?php echo $critico -> getApellido() ?></a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
        <a class="dropdown-item" href="index.php?pid=<?php echo base64_encode("presentacion/critico/editarFotoCritico.php") ?>">Editar Foto</a> 
        </div></li>
      <li class="nav-item active"><a class="nav-link" href="index.php?cerrarSesion=true">Cerrar Sesion</a></li>
    </ul>
  </div>
</nav>