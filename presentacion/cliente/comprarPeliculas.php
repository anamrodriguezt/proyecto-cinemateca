<?php
$director = new director();
$pelicula = new pelicula();
$cantidad = 5;
if(isset($_GET["cantidad"])){
    $cantidad = $_GET["cantidad"];
}
$pagina = 1;
if(isset($_GET["pagina"])){
    $pagina = $_GET["pagina"];
}

$peliculas = $pelicula -> consultarTodos();
$totalRegistros = $pelicula -> consultarCantidad();
$totalPaginas = intval($totalRegistros/$cantidad);
if($totalRegistros%$cantidad != 0){
    $totalPaginas++;
}
if(isset($_POST["añadirCarrito"])){
	if (isset($_SESSION["idCarrito"])) {
		$carrito=new Carrito($_SESSION["idCarrito"]);
		$carrito->consultar();
		
	  }
		 else{
		$carrito=new Carrito("",date("Y-m-d"),0,$_SESSION["id"]);
		$carrito->insertar();
		$carrito->consultarUltimo();
		$_SESSION["idCarrito"]=$carrito->getIdCarrito(); 	
		 }
	$peliculaaa=new Pelicula($_POST["idPelicula"]);
	$peliculaaa->consultar();
	$carrito_pelicula=new Carrito_pelicula("",$_POST["cantidadComprada"], $peliculaaa->getPrecio(),$peliculaaa->getId(),
	$carrito->getIdCarrito());
	$carrito_pelicula->insertar();
	$carrito->setValorFinal($carrito->getValorFinal()+$carrito_pelicula->getPrecio());
	$carrito->actualizar();
}
?>
<div class="container mt-3">
	<div class="row">
		<div class="col">
            <div class="card">
				<div class="card-header text-black bg-warning ">
					<h4>Peliculas en cartelera</h4>
				</div>
				<div class="text-right">Resultados <?php echo (($pagina-1)*$cantidad+1) ?> al <?php echo (($pagina-1)*$cantidad)+count($peliculas) ?> de <?php echo $totalRegistros ?> registros encontrados</div>
              	<div class="card-body">
					<table class="table table-hover table-striped table-responsive-md">
						<tr>
							<th>#</th>
							<th>Nombre</th>
							<th>Existencias</th>
							<th>Precio</th>
							<th>Imagen</th>
							<th>Cantidad</th>
							<th>Carrito</th>
						</tr>
						<?php 
						$i=1;
						foreach($peliculas as $peliculaActual){
						    echo "<tr>";
						    echo "<td>" . $i . "</td>";
						    echo "<td>" . $peliculaActual -> getNombre() . "</td>";
						    echo "<td>" . $peliculaActual -> getCantidad() . "</td>";
//							echo "<td><div id='accion". $peliculaActual -> getId() . "'><p>". $peliculaActual -> getCantidad() . "</p>";
							echo "<td>" . $peliculaActual -> getPrecio() . "</td>";
							echo "<td> <img src='carterapeliculas/" . $peliculaActual -> getImagen() . "' height='80px'>"  . "</td>";
							echo "<td nowrap>
									<form action='index.php?pid=" . base64_encode("presentacion/cliente/comprarPeliculas.php") . "' method='POST'>
									    <input type='hidden' value='".$peliculaActual -> getId()."' name='idPelicula'> 
										<input type='number' value='1' name='cantidadComprada'> 
										</td>";
							echo "<td> <button type='submit' name='añadirCarrito' ><i class='fas fa-shopping-cart' data-toggle='tooltip' data-placement='bottom' title='Añadir al carrito'></i></button> </td>";
						    echo "</form> </tr>";
						    $i++;
						}
						?>
					</table>
				</div>
				 <div class="card-footer text-muted">
	 
              </div>
            </div>
		</div>
	</div>
</div>



 <script>
   function Funcion() {
  <?php 
   array_push($arrayid,$peliculaActual->getId());                                   		  
    array_push($arraycant,$peliculaActual->getCantidad());
                                   		   
                                      ?>
                                      
                                    }
                                    </script>
