<?php
 $e=false;
if(isset($_SESSION["idCarrito"])){
    $e=true;
   $carrito=new Carrito($_SESSION["idCarrito"]); 
   $carrito->consultar();
   $ids_peliculas=$carrito->consultarPeliculasEnCarrito();
   $carrito_peliculas=array();
   foreach($ids_peliculas as $id_peliculaActual){
        $peliculaEnCarrito=new Carrito_pelicula($id_peliculaActual);
        $peliculaEnCarrito->consultar();
        array_push($carrito_peliculas,$peliculaEnCarrito);
        
   }
   
}
?>
<br></br>
<div class="container">
	<div class="row">
		<div class="col-12">
			<div class="card">
				<div class="card-header bg-info text-dark">Consultar Peliculas en Carrito</div>
				<div class="card-body">
					<table class="table table-striped table-hover">
						<thead>
							<tr>						
								<th scope="col">Imagen</th>
								<th scope="col">Nombre</th>
								<th scope="col">Genero</th>
								<th scope="col">Cantidad</th>
								<th scope="col">valor total por pelicula</th>
							</tr>
						</thead>
						<tbody>
						<?php
						if($e){
                            foreach ($carrito_peliculas as $cp) {
                                echo "<tr>";
                                $pelicula=new Pelicula($cp->getIdPelicula());
                                $pelicula->consultar();
                                echo "<td>" . (($pelicula->getImagen()!="")?"<img src='carterapeliculas/" . $pelicula->getImagen() . "' height='50px'>":"") . "</td>";
                                echo "<td>" . $pelicula->getNombre() . "</td>";
                                echo "<td>" . $pelicula->getGenero()->getNombre() . "</td>";
                                echo "<td>" . $cp->getCantidad(). "</td>";
                                echo "<td>" . $cp->getPrecio()."</td>";                        
                            
                            }
                            echo "<tr><td colspan='7'>..." . count($carrito_peliculas) . " registros encontrados...</td></tr>";
                        }
                        ?>
						</tbody>                        
					</table>
                    <h1>valor total:<?php echo $carrito->getValorFinal() ?> </h1>
				</div>
			</div>
		</div>
	</div>
</div>