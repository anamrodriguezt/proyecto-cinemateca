<?php 
$idCliente = $_GET["idCliente"];
$cliente = new Cliente($idCliente);
$cliente -> consultar();
if($cliente -> getEstado() == 1){
    $cliente -> cambiarEstado(0);
    echo "<i class='fas fa-times-circle' data-toggle='tooltip' data-placement='bottom' title='Deshabilitado'></i>";
}else{
    $cliente -> cambiarEstado(1);
    echo "<i class='fas fa-check-circle' data-toggle='tooltip' data-placement='bottom' title='Habilitado'></i>";
}
?>