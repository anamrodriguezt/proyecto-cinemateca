<?php 
include "presentacion/cliente/comprarPeliculas.php";
$error = 0;
$registrado = false;
if (isset($_SESSION["idCarrito"])) {
    $carrito=new Carrito($_SESSION["idCarrito"],'fecha','valorfinal', 'idCliente');
    $carrito->consultar();
    $carrito_pelicula=new Carrito_pelicula("",'carrito'.getId(), 'fecha'.getFecha(),'valorfinal'.getValorfinal(),
     'idCliente'.getidCliente());
    $carrito_pelicula->insertar();
  }
     else{
    $carrito->insertar();
    $carrito->consultarUltimo(); 
    $carrito_pelicula = new  Carrito_pelicula("",'carrito'.getId(), 'fecha'.getFecha(),'valorfinal'.getValorfinal(),
     'idCliente'.getidCliente());
         $carrito_pelicula->insertar();
     }
    

?>
<div class="container mt-3">
	<div class="row">
		<div class="col-lg-3 col-md-0"></div>
		<div class="col-lg-6 col-md-12">
            <div class="card">
				<div class="card-header text-black bg-warning">
					<h4 class=text-center>Registro de peliculas en el carrito</h4>
				</div>
              	<div class="card-body">
        			<form action="index.php?pid=<?php echo base64_encode("presentacion/cliente/verCarrito.php") ?>" method="post">
        				<div class="form-group">
    						<a href="cantidad" type="cantidad" class="form-control" placeholder="cantidad" required"></a> 
    					</div>
        				<div class="form-group">
                            <a href="precio" type="precio" class="form-control" placeholder="precio" required"></a>
    					</div>
        			<div class="form-group">
    						<input name="guardar" type="guardar" class="form-control btn btn-warning">
    					</div>	    					
        			</form>     
        			<?php if($error == 1){ ?>
					<div class="alert alert-danger alert-dismissible fade show" role="alert">
						 <?php echo $peliculas ?> ya se encuentra registrado.
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					</div>
					<?php } else if($registrado) { ?>
					<div class="alert alert-success alert-dismissible fade show" role="alert">
						la pelicula fue registrada exitosamente.
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					</div>
					<?php } ?>
            	</div>
            </div>		
		</div>
	</div>
</div>

