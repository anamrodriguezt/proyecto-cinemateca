<?php
$utilidades = new UtilidadesDAO();
$utilidades -> console_log($_SESSION["rol"]);
$utilidades -> console_log($_SESSION["rol"] == "Administrador");
$administrador = new Administrador($_SESSION['id']);
$administrador->consultar();

include 'presentacion/inicio/menuAdministrador.php';


if($_SESSION["rol"] == "Administrador"){
    $critico = new Critico();
    $criticos = $critico -> consultarTodos();
    ?>
    <div class="container">
        <div class="row mt-3">
            <div class="col">
                <div class="card">
                    <div class="card-header">
                        <h3>Consultar critico</h3>
                    </div>
                    <div class="card-body">
                        <table class="table table-striped table-hover">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Nombre</th>
                                    <th>Apellido</th>
                                    <th>Correo</th>
                                    <th>Estado</th>
                                    <th>Foto</th>
                                    <th>Servicios</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php 
                            $i = 1;

                            foreach ($criticos as $criticoActual){
                                echo "<tr>";
                                echo "<td>" . $i++ . "</td><td>" . $criticoActual -> getNombre() . "</td><td>" . $criticoActual -> getApellido() . "</td><td>" . $criticoActual -> getCorreo() . "</td>";
                                echo "<td><div id='estado" . $criticoActual -> getId() . "'>" . (($criticoActual -> getEstado()==1)?"<i class='fas fa-check-circle' data-toggle='tooltip' data-placement='bottom' title='Habilitado'></i>":"<i class='fas fa-times-circle' data-toggle='tooltip' data-placement='bottom' title='Deshabilitado'></i>") . "<div></td>";
                                echo "<td><img src='" . $criticoActual -> getFoto() . "' width='50px' /></td>";
                                echo "<td nowrap><a href='indexModal.php?pid=" . base64_encode("presentacion/administrador/modalCritico.php") . "&id=" . $criticoActual -> getId() . "' data-toggle='modal' data-target='#modalCritico'><i class='fas fa-eye' data-toggle='tooltip' data-placement='bottom' title='Ver detalles'></i></a> <a href='#'><i id='cambiarEstado" . $criticoActual -> getId() . "' class='fas fa-user-edit' data-toggle='tooltip' data-placement='bottom' title='Cambiar Estado'></i></a> ";
                                echo "<a href='index.php?pid=" . base64_encode("presentacion/critico/editarFotoCritico.php") . "&id" . $criticoActual -> getId() ."'><i class='fas fa-camera' data-toggle='tooltip' data-placement='bottom' title='Cambiar Foto'></i></a></td>";
                                echo "</tr>";       
                            }                       
                            ?>                      
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <script>
    $(document).ready(function(){
    <?php 
    $i = 1;
    foreach ($criticos as $criticoActual){
        echo "\t$(\"#cambiarEstado" . $criticoActual -> getId() . "\").click(function(){\n";
        echo "\t\turl = \"indexAjax.php?pid=" . base64_encode("presentacion/critico/cambiarEstadoCriticoAjax.php") . "&id=" . $criticoActual -> getId() . "\"\n";
        echo "\t\t$(\"#estado" . $criticoActual -> getId() . "\").load(url);\n";
        echo "\t});\n\n";
    }   
    ?>
    });
    </script>
    
    <div class="modal fade" id="modalCritico" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg">
        <div class="modal-content" id="modal-content">      
        </div>
      </div>
    </div>
    
    <script>
    $('body').on('show.bs.modal', '.modal', function (e) {
        var link = $(e.relatedTarget);
        $(this).find(".modal-content").load(link.attr("href"));
    });
    </script>
<?php 
} else {
    echo "Lo siento. Usted no tiene permisos";
}
?>

