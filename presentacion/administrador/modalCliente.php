<?php 
if($_SESSION["rol"] == "administrador"){
    $cliente = new Cliente($_GET["idCliente"]);
    $cliente -> consultar();
    ?>
    <div class="modal-header">
    	<h5 class="modal-title" id="exampleModalLabel"><?php echo $cliente -> getNombre() . " " . $cliente -> getApellido() ?></h5>
    	<button type="button" class="close" data-dismiss="modal"
    		aria-label="Close">
    		<span aria-hidden="true">&times;</span>
    	</button>
    </div>
    <div class="modal-body"><img src="<?php echo $cliente -> getFoto() ?>" width="100%"></div>
<?php 
} else {
    echo "Lo siento. Usted no tiene permisos";
}
?>



