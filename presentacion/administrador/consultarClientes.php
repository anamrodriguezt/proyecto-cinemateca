<?php
$utilidades = new UtilidadesDAO();
$utilidades -> console_log($_SESSION["rol"]);
$utilidades -> console_log($_SESSION["rol"] == "Administrador");
$administrador = new Administrador($_SESSION['id']);
$administrador->consultar();
include 'presentacion/inicio/menuAdministrador.php';


if($_SESSION["rol"] == "Administrador"){
    $cliente = new Cliente();
    $clientes = $cliente -> consultarTodos();
    ?>
    <div class="container">
        <div class="row mt-3">
            <div class="col">
                <div class="card">
                    <div class="card-header">
                        <h3>Consultar Cliente</h3>
                    </div>
                    <div class="card-body">
                        <table class="table table-striped table-hover">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Nombre</th>
                                    <th>Apellido</th>
                                    <th>Correo</th>
                                    <th>Estado</th>
                                    <th>Foto</th>
                                    <th>Servicios</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php 
                            $i = 1;
                            foreach ($clientes as $clienteActual){
                                echo "<tr>";
                                echo "<td>" . $i++ . "</td><td>" . $clienteActual -> getNombre() . "</td><td>" . $clienteActual -> getApellido() . "</td><td>" . $clienteActual -> getCorreo() . "</td>";
                                echo "<td><div id='estado" . $clienteActual -> getId() . "'>" . (($clienteActual -> getEstado()==1)?"<i class='fas fa-check-circle' data-toggle='tooltip' data-placement='bottom' title='Habilitado'></i>":"<i class='fas fa-times-circle' data-toggle='tooltip' data-placement='bottom' title='Deshabilitado'></i>") . "<div></td>";
                                echo "<td><img src='" . $clienteActual -> getFoto() . "' width='50px' /></td>";
                                echo "<td nowrap><a href='indexModal.php?pid=" . base64_encode("presentacion/administrador/modalCliente.php") . "&id=" . $clienteActual -> getId() . "' data-toggle='modal' data-target='#modalCliente'><i class='fas fa-eye' data-toggle='tooltip' data-placement='bottom' title='Ver detalles'></i></a> <a href='#'><i id='cambiarEstado" . $clienteActual -> getId() . "' class='fas fa-user-edit' data-toggle='tooltip' data-placement='bottom' title='Cambiar Estado'></i></a> ";
                                echo "<a href='index.php?pid=" . base64_encode("presentacion/cliente/editarFotoCliente.php") . "&id" . $clienteActual -> getId() ."'><i class='fas fa-camera' data-toggle='tooltip' data-placement='bottom' title='Cambiar Foto'></i></a></td>";
                                echo "</tr>";       
                            }                       
                            ?>                      
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <script>
    $(document).ready(function(){
    <?php 
    $i = 1;
    foreach ($clientes as $clienteActual){
        echo "\t$(\"#cambiarEstado" . $clienteActual -> getId() . "\").click(function(){\n";
        echo "\t\turl = \"indexAjax.php?pid=" . base64_encode("presentacion/cliente/cambiarEstadoClienteAjax.php") . "&id=" . $clienteActual -> getId() . "\"\n";
        echo "\t\t$(\"#estado" . $clienteActual -> getId() . "\").load(url);\n";
        echo "\t});\n\n";
    }   
    ?>
    });
    </script>
    
    <div class="modal fade" id="modalCliente" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg">
        <div class="modal-content" id="modal-content">      
        </div>
      </div>
    </div>
    
    <script>
    $('body').on('show.bs.modal', '.modal', function (e) {
        var link = $(e.relatedTarget);
        $(this).find(".modal-content").load(link.attr("href"));
    });
    </script>
<?php 
} else {
    echo "Lo siento. Usted no tiene permisos";
}
?>

