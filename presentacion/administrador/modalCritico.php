<?php 
if($_SESSION["rol"] == "administrador"){
    $critico = new Critico($_GET["idCritico"]);
    $critico -> consultar();
    ?>
    <div class="modal-header">
    	<h5 class="modal-title" id="exampleModalLabel"><?php echo $critico -> getNombre() . " " . $critico -> getApellido() ?></h5>
    	<button type="button" class="close" data-dismiss="modal"
    		aria-label="Close">
    		<span aria-hidden="true">&times;</span>
    	</button>
    </div>
    <div class="modal-body"><img src="<?php echo $critico -> getFoto() ?>" width="100%"></div>
<?php 
} else {
    echo "Lo siento. Usted no tiene permisos";
}
?>
