<?php
require_once 'logica/Idioma.php';
require_once 'logica/Director.php';
require_once 'logica/Reseña.php';
$critico = new Critico($_SESSION['id']);
$critico->consultar();
$pelicula = new Pelicula();
$peliculas = $pelicula->consultarTodos();
//$idioma = new Idioma();
/*$director = new Director();*/

?>
<br></br>
<div class="container">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header bg-info text-dark">Creacion de reseña de las peliculas</div>
                <div class="card-body">
                    <table class="table table-striped table-hover">

                        <thead>
                            <tr>                        
                                <th scope="col">Imagen</th>
                                <th scope="col">Nombre</th>
                                <th scope="col">Genero</th>
                                <th scope="col">Idioma</th>
                                <th scope="col">Director</th>
                                <th scope="col">Reseña</th>

                            </tr>
                        </thead>
                        <tbody>
                        <?php
                        



    foreach ($peliculas as $p) {
        echo "<tr>";
        echo "<td>" . (($p->getImagen()!="")?"<img src='carterapeliculas/" . $p->getImagen() . "' height='50px'>":"") . "</td>";
        echo "<td>" . $p->getNombre() . "</td>";
        echo "<td>" . $p->getGenero()->getNombre() . "</td>";
        echo "<td>" . $p->getIdioma()->getNombre(). "</td>";
        echo "<td>" . $p->getDirector()->getNombre()."</td>";


        echo "<td >
        <form action= presentacion/critico/form-result.php target=blank>
        <p><textarea name=descripcion cols=40 rows=4 placeholder=Comparte opinion! ></textarea></p>
        <p><input type=submit value=Enviar mensaje class='alert alert-success' role='alertr'></p> 
        </form>
        </td>";

         
    
         /*  <p>Usuario: <?php echo $administrador -> getNombre() . " " . $administrador -> getApellido() ?></p> */
    }
    echo "<tr><td colspan='7'>..." . count($peliculas) . " registros encontrados...</td></tr>"?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


<script>
    $('body').on('show.bs.modal', '.modal', function (e) {
        var link = $(e.relatedTarget);
        $(this).find(".modal-content").load(link.attr("href"));
    });
</script>

